while (<>) {
	last if /checksyscalls/;
}

while (<>) {
	last if /^  CC/;
}

$errors = "";
$keep = 1;
$result = "ERRORS:";

while (<>) {
	if (/VDSOSYM/) {
		$errors = "";
		$err = "";
	}

	if (/End git build/) {
		$errors .= $err if $keep;
		if (length($errors)) {
			$result = "WARNINGS:";
		}
		else {
			$result = "OK";
		}
		last;
	}
	$lines .= $_;
	if (/^  CC/ || /^  LD/ || /^  AR/ || /^  HOSTCC/ || /^  LEX/ || /^  YACC/ ||
	    /^  MKELF/ || /^  UPD/ || /^  HOSTLD/ || /^  CHECK/ || /^make/ || /^  VDSO/ || /^  OBJCOPY/) {
		$errors .= $err if $keep;
		$keep = 1;
		$err = "";
		next;
	}
	# Ignore messages we can't do anything about
	if (/Assembler messages/ || /rdhi, rdlo and rm must all be different/ || /too hairy/ ||
	    /uaccess_64.h.*cast removes address space '__user' of expression/ ||
	    /note:.*through.*uaccess.h/) {
		$errors .= $err if $keep;
		$keep = 1;
		$err = "";
		next;
	}
	$err .= $_;
}
printf "%s\n", $result;
if (length($errors)) {
	print "\n" . $errors . "\n";
}
